## Welcome to Ocean! 

Thanks for looking at Ocean - a free, open source, CLI application that makes travelling around London easier. 

This README goes into detail on how to use the application and general information.


## Technical Information

Ocean is written in Go. The reason why I chose Go for writing this application is because of the following reasons:

- Simplicity
- Speed
- Easy to distribute software
- Any unused memory is freed automatically
- Fast growing community
- A popular language

The application is designed to run in the CLI and can be run by:
 - Clicking on the executable
 - Navigating to the location of the file in the terminal and running it.

The application uses Transport for London's Unified API to retrieve information.


## General Use

The application allows you to do the following:
- View Underground line status and stops on the line.
- View road status.
- Get directions from one Underground station to another Underground station.
   - This will also display the current cost of the journey.
   - Displays the Peak fare.
   - The Off Peak fare.
   - The travel time.
   - Detailed steps.
   - Disruptions for each step (if there are any disruptions).
- View software information.

To run a menu option, type in the associated number or letter in the bracket shown on the main menu.


## API Key Information

You can use Ocean without API Keys to find journey information, but you may find that Ocean won't be able to retrieve any data after several attempts. TfL restricts the number of calls a user can make to the API without an appropriate key.

This will normally reset itself and if you call for a journey once, it's unlikely you will encounter this problem.

You can configure the session and set it up with your own key from the TfL website.


## What's in Ocean v0.1?

This is the first initial release of Ocean, which has some basic functionality.


## Download

You have a few ways of using Ocean on your own machine.

- You can fork the repo and run the Go program by using 'go run Ocean.go'. **You will need Go installed for running Ocean this way!**
- You can download the executable and run it on your machine.
    - For Windows users - use the 'Ocean.exe' file in the Windows folder.
    - For Mac/Linux users - use the 'Ocean' file in the Mac OSX - Linux folder.


## Bug reporting

As this software is in active development (and in early access somewhat), there will be bugs. If you find any, please let me know by dropping me an email.


## About

Ocean is written by Joshua Blewitt - who is an aspiring junior developer. Why not check out my [website](https://joshblewitt.dev/) for contact information and get in touch?
